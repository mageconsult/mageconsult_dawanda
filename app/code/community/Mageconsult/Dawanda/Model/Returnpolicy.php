<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 05.02.16
 * Time: 00:21
 */ 
class Mageconsult_Dawanda_Model_Returnpolicy extends Mageconsult_Dawanda_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/returnpolicy');
    }


    public function updateDataFromDawanda()
    {

        $query  = Mage::getModel('mageconsult_dawanda/query_returnpolicy');
        $returnpolicies = $query->query();

        foreach ($returnpolicies as $returnpolicy) {

            $attributes = $returnpolicy->attributes();
            $id = (int) $attributes->id;
            $text = (string) $attributes->text;
            $title = (string) $attributes->title;

            $returnpolicyModel = Mage::getModel('mageconsult_dawanda/returnpolicy');
            $returnpolicyModel->load($id, 'returnpolicy_id');

            $returnpolicyModel->setData('returnpolicy_id', $id);
            $returnpolicyModel->setData('language', 'de');
            $returnpolicyModel->setData('title', $title);
            $returnpolicyModel->setData('text', $text);
            $returnpolicyModel->setData('created_at', now());
            $returnpolicyModel->setData('updated_at', now());

            try {
                $returnpolicyModel->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }
}