<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Synctype extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => Mageconsult_Dawanda_Model_Product::SYNC_DISABLED, 'label' => Mage::helper('mageconsult_dawanda')->__('disabled')),
            array('value' => Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA, 'label' => Mage::helper('mageconsult_dawanda')->__('stock data')),
            array('value' => Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA, 'label' => Mage::helper('mageconsult_dawanda')->__('full data')),
        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }

        return $result;
    }
}

