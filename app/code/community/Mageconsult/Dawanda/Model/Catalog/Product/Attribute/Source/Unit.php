<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Unit extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('mageconsult_dawanda')->__('')),
            array('value' => 'm', 'label' => Mage::helper('mageconsult_dawanda')->__('m')),
            array('value' => 'g', 'label' => Mage::helper('mageconsult_dawanda')->__('g')),
            array('value' => 'kg', 'label' => Mage::helper('mageconsult_dawanda')->__('kg')),
            array('value' => 'l', 'label' => Mage::helper('mageconsult_dawanda')->__('l')),
            array('value' => 'piece', 'label' => Mage::helper('mageconsult_dawanda')->__('Stück')),

        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }


    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }

        return $result;
    }
}

