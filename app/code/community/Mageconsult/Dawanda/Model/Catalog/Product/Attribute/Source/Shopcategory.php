<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Shopcategory extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        $language = Mage::getStoreConfig('dawanda/general/language');

        /** @var Mageconsult_Dawanda_Model_Resource_Shopcategory_Collection $collection */
        $collection = Mage::getModel('mageconsult_dawanda/shopcategory')
            ->getCollection()
            ->addFieldToFilter('language', $language)
            ;

        $options = array(array('value' => '', 'label' => ''));
        /** @var Mageconsult_Dawanda_Model_Shopcategory $item */
        foreach ($collection as $item) {
            $options[] = array(
                'value' => $item->getData('shopcategory_id'),
                'label' => $item->getData('name'));
        }
        return $options;
    }


    public function getAllOptions()
    {
        return $this->toOptionArray();
    }


    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }

        return $result;
    }

}

