<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Category extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $language = Mage::getStoreConfig('dawanda/general/language');

        /** @var Mageconsult_Dawanda_Model_Resource_Category_Collection $collection */
        $collection = Mage::getModel('mageconsult_dawanda/category')
            ->getCollection()
            ->addFieldToFilter('language', $language);

        $usedCategories = Mage::getModel('mageconsult_dawanda/product')
            ->getCollection()
           ;

        $usedCategories ->getSelect()
            ->columns('COUNT(*) AS countCategory')
            ->group('category_id')
            ->order('category_id ASC');

        $lookup = array();
        #$options = array(array('value' => '0', 'label' => 'none'));
        $options = array();
        /** @var Mageconsult_Dawanda_Model_Category $item */
        foreach ($collection as $item) {
            $options[] = array(
                'value' => $item->getData('category_id'),
                'label' => $item->getData('level1') . ' > ' . $item->getData('level2') . ' > ' . $item->getData('level3')
            );

            $lookup[$item->getData('category_id')] = $item->getData('level1') . ' > ' . $item->getData('level2') . ' > ' . $item->getData('level3');
        }

        $prefer = [];
        foreach ($usedCategories as $key => $item) {
            $prefer[] = array(
                'value' => $item->getData('category_id'),
                'label' => $lookup[$item->getData('category_id')] . ' (' . $item->getData('countCategory') . ')'
            );
        }
        $prefer[] = array('value' => '0', 'label' => '----- none -----');

        return array_merge($prefer, $options);
    }


    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }

        return $result;
    }
}

