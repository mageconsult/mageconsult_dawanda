<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Observer
{

    public function cataloginventoryStockItemSaveAfter($observer)
    {
        $stockItem = $observer->getEvent()->getItem();
        $product = Mage::getModel('catalog/product')->load($stockItem->getProductId());

        if ($product->getId())
        {
            $enableSync = $product->getData('dawanda_enable_sync');

            switch ($enableSync)
            {
                case Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA:
                case Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA:
                    $this->_queueStockItem($product, Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA);
                    break;
            }
        }
    }


    /**
     * @param $observer
     */
    public function catalogProductSaveAfter($observer)
    {
        $product = $observer->getEvent()->getProduct();

        $enableSync = $product->getData('dawanda_enable_sync');

        switch ($enableSync)
        {
            case Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA:
                $this->_queueProduct($product, Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA);
                break;

            case Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA:
                $this->_queueProduct($product, Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA);
                break;
        }
    }

    protected function _queueStockItem(Mage_Catalog_Model_Product $product, $state)
    {
        $queue = Mage::getModel('mageconsult_dawanda/queue');

        $queue->setType('stock');
        $queue->setState(Mageconsult_Dawanda_Model_Log::OPEN);
        $queue->setEntityId($product->getId());
        $queue->setCreatedAt(now());
        $queue->save();
    }

    protected function _queueProduct(Mage_Catalog_Model_Product $product, $state)
    {
        $queue = Mage::getModel('mageconsult_dawanda/queue');

        $queue->setType('product');
        $queue->setState(Mageconsult_Dawanda_Model_Log::OPEN);
        $queue->setEntityId($product->getId());
        $queue->setCreatedAt(now());
        $queue->save();
    }
}