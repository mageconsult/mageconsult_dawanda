<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Typology extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '0', 'label' => Mage::helper('mageconsult_dawanda')->__('physisches Produkt')),
            array('value' => '1', 'label' => Mage::helper('mageconsult_dawanda')->__('digitales Produkt')),
            array('value' => '2', 'label' => Mage::helper('mageconsult_dawanda')->__('Dienstleistung')),
        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }

        return $result;
    }
}

