<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Mailable extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            #array('value' => '', 'label' => Mage::helper('mageconsult_dawanda')->__('')),
            array('value' => '11', 'label' => Mage::helper('mageconsult_dawanda')->__('1-2 Tage')),
            array('value' => '12', 'label' => Mage::helper('mageconsult_dawanda')->__('3-5 Tage')),
            array('value' => '13', 'label' => Mage::helper('mageconsult_dawanda')->__('6-9 Tage')),
            array('value' => '14', 'label' => Mage::helper('mageconsult_dawanda')->__('10-14 Tage')),
            array('value' => '15', 'label' => Mage::helper('mageconsult_dawanda')->__('15-21 Tage')),
            array('value' => '21', 'label' => Mage::helper('mageconsult_dawanda')->__('4 Wochen')),
            array('value' => '22', 'label' => Mage::helper('mageconsult_dawanda')->__('5 Wochen')),
            array('value' => '23', 'label' => Mage::helper('mageconsult_dawanda')->__('6 Wochen')),
            array('value' => '24', 'label' => Mage::helper('mageconsult_dawanda')->__('7 Wochen')),
            array('value' => '25', 'label' => Mage::helper('mageconsult_dawanda')->__('8 Wochen')),
            array('value' => '26', 'label' => Mage::helper('mageconsult_dawanda')->__('9 Wochen')),
            array('value' => '27', 'label' => Mage::helper('mageconsult_dawanda')->__('10 Wochen')),
        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {

        $options = $this->toOptionArray();

        $result = array();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }
        #array_shift($result);

        return $result;
    }

}

