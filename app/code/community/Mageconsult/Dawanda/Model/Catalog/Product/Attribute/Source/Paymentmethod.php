<?php

class Mageconsult_Dawanda_Model_Catalog_Product_Attribute_Source_Paymentmethod extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $language = Mage::getStoreConfig('dawanda/general/language');

        /** @var Mageconsult_Dawanda_Model_Resource_Paymentmethod_Collection $collection */
        $collection = Mage::getModel('mageconsult_dawanda/paymentmethod')
            ->getCollection()
            ->addFieldToFilter('language', $language)
            ;

        $options = array(array('value' => '', 'label' => ''));
        /** @var Mageconsult_Dawanda_Model_Paymentmethod $item */
        foreach ($collection as $item) {
            $options[] = array(
                'value' => $item->getData('paymentmethod_id'),
                'label' => $item->getData('title'));
        }
        return $options;
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }
}

