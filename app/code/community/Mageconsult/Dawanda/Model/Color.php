<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 22:10
 */ 
class Mageconsult_Dawanda_Model_Color extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/color');
    }

    public function updateDataFromDawanda()
    {

        $query  = Mage::getModel('mageconsult_dawanda/query_color');
        $colors = $query->query();

        foreach ($colors as $color) {

            $attributes = $color->attributes();
            $hexCode = (string) $attributes->hex_code;
            $name = (string) $attributes->name;

            $colorModel = Mage::getModel('mageconsult_dawanda/color');
            $colorModel->load($hexCode, 'hex_code');

            $colorModel->setData('hex_code', $hexCode);
            $colorModel->setData('language', 'de');
            $colorModel->setData('name', $name);
            $colorModel->setData('created_at', now());
            $colorModel->setData('updated_at', now());

            try {
                $colorModel->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }
}