<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Query_Order extends Mageconsult_Dawanda_Model_Query_Abstract
{

    const URL = 'https://dawanda.com/seller_api/orders?v=1.1';

    /**
     *
     */
    protected function _construct()
    {
        return parent::_construct();
    }

    /**
     * @return mixed|SimpleXMLElement
     */
    public function query()
    {

        $from = strtotime('01.01.2014');
        $url = self::URL;
        $url .= '&from=' . $from;

        $result = parent::callAPI('', $url, '', array('X-Dawanda-Auth: ' . $this->getApiKey()));
        $result = new SimpleXMLElement($result, 0, false, '', true);
        return $result;

    }
}