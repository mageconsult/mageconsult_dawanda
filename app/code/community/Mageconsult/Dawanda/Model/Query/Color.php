<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Query_Color extends Mageconsult_Dawanda_Model_Query_Abstract
{

    const URL = 'https://dawanda.com/seller_api/colors?language=';

    protected function _construct()
    {
        return parent::_construct();
    }

    public function query() {

        $result = parent::callAPI('', self::URL . 'de', '', array('X-Dawanda-Auth: ' . $this->getApiKey()));

        $result = new SimpleXMLElement($result, 0, false, self::DAWANDA_NAMESPACE, true);

        return $result;

    }

}