<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Query_Abstract extends Mage_Core_Model_Abstract
{

    const DAWANDA_NAMESPACE = 'dawanda';


    protected $_apiKey;

    protected $_statusCode;
    protected $_statusMessage;

    protected function _construct()
    {

        $this->setApiKey();

    }

    public function setApiKey() {

        $this->_apiKey = Mage::getStoreConfig('dawanda/general/apikey');
    }

    public function getApiKey() {
        return $this->_apiKey;
    }


    public function callAPI($method, $url, $data = false, $header = '')
    {
        $curl = curl_init();

        switch (strtoupper($method))
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if (is_array($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        $result = curl_exec($curl);

        if (!$result) {

        }

        curl_close($curl);

        return $result;
    }



}