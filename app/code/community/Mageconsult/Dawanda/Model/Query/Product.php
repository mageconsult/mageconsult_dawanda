<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Query_Product extends Mageconsult_Dawanda_Model_Query_Abstract
{

    const URL = 'https://dawanda.com/seller_api/products?v=1.1&limit=9999';

    protected function _construct()
    {
        return parent::_construct();
    }

    public function query()
    {

        $result = parent::callAPI('', self::URL, '', array('X-Dawanda-Auth: ' . $this->getApiKey()));

        $result = new SimpleXMLElement($result, 0, false, '', true);

        return $result;

    }

    public function xxx_createProduct()
    {

        $client = new Varien_Http_Client();
        $client->setUri('https://dawanda.com/seller_api/products?v=1.1&limit=9');
        $client->setMethod('PUT');
        $client->setConfig(array(
            'maxredirects' => 0,
            'timeout'      => 30,
        ));
        $client->setHeaders(array(
            'X-Dawanda-Auth' => $this->getApiKey()
        ));

        $response = $client->request();
        $result   = $response->getBody();
        $result   = new SimpleXMLElement($result, 0, false, '', true);

        return $result;
    }

    public function createDawandaProduct($product)
    {

        $sku = $product->getSku();
        $xml = $this->_createXML($product);

        // log request
        $log = Mage::getModel('mageconsult_dawanda/log');
        $log->setRequest($xml)
            ->setCreatedAt(now())
            ->save();

        $client = new Varien_Http_Client();
        $client->setUri('https://dawanda.com/seller_api/products/' . $sku . '?v=1.1');
        $client->setMethod('POST');

        $client->setConfig(array(
            'maxredirects'   => 10,
            'timeout'        => 300,
            'customrequest'  => 'PUT',
            'returntransfer' => true,
            'postfields'     => $xml,
        ));
        $client->setHeaders(array(
            'X-Dawanda-Auth' => $this->getApiKey(),
            'Content-Type: text/xml'
        ));

        #$client->setParameterPost($xml);

        #$response = $client->request();
        #$result   = $response->getBody();
        #$result   = new SimpleXMLElement($result, 0, false, '', true);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://dawanda.com/seller_api/products/' . $sku . '?v=1.1');
        #curl_setopt( $ch, CURLOPT_PUT, true );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'X-Dawanda-Auth: f8da3e637ee9b472e662aaf421b42debd0a27085'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($ch);

        // log response
        $log->setResponse($result)
            ->setUpdatedAt(now());

        curl_close($ch);
        $result = new SimpleXMLElement($result, 0, false, '', true);

        // log errortext
        $log->setErrortext($result->message)
        ->save();

        return $result;
    }


    /**
     * @param $product
     * @return mixed
     */
    protected function _createXML($product)
    {

        $productXML = new SimpleXMLElement('<dawanda:product xmlns:dawanda="http://dawanda.com/api/resources/schemas/v1.1/Product"></dawanda:product>', 0, false, Mageconsult_Dawanda_Model_Query_Abstract::DAWANDA_NAMESPACE, true);
        $productXML->addAttribute('sku', $product->getSku());
        $productXML->addAttribute('auto_renew_enabled', $product->getData('dawanda_auto_renew_enabled'));
        $productXML->addAttribute('mailable_in', $product->getData('dawanda_mailable_in_option'));
        $productXML->addAttribute('category_id', $product->getData('dawanda_category1'));
        $productXML->addAttribute('shop_category_id', $product->getData('dawanda_shopcategory'));
        $productXML->addAttribute('shipping_profile_id', $product->getData('dawanda_shippingprofile'));
        $productXML->addAttribute('return_policy_id', $product->getData('dawanda_returnpolicy'));

        if (!empty($product->getData('dawanda_color1'))) {
            $productXML->addAttribute('color_1', $product->getData('dawanda_color1'));
        }
        if (!empty($product->getData('dawanda_color2'))) {
            $productXML->addAttribute('color_2', $product->getData('dawanda_color2'));
        }

        // product texts
        $productTexts = $productXML->addChild('dawanda:product_texts');

        $productText = $productTexts->addChild('dawanda:product_text');
        $productText->addAttribute('language', 'de');

        $productText->addChild('dawanda:title', $product->getName());
        if (!empty($product->getData('dawanda_tags_as_text'))) {
            $productText->addChild('dawanda:tags', $product->getData('dawanda_tags_as_text'));
        }
        $productText->addChild('dawanda:basic_attributes', $product->getData('dawanda_basic_attributes'));
        $productText->addChild('dawanda:description', $this->_parseDescription($product->getDescription()));

        // images
        $productImages = $productXML->addChild('dawanda:images');
        $i = 1;
        foreach ($product->getMediaGalleryImages() as $image) {
            if (!$image->getDisabled()) {
                $url          = $image->getUrl();
                $url          = str_replace('stoffwerft.dev', 'stoffwerft.com', $url);
                $productImage = $productImages->addChild('dawanda:image');
                $productImage->addAttribute('uri', $url);
                $i++;
            }
            if ($i > 4) {
                break;
            }
        }

        // product pricing
        $productPricing = $productXML->addChild('dawanda:pricing_information');

        $productBasepriceInformation = $productPricing->addChild('dawanda:base_price_information');
        $productBasepriceInformation->addChild('dawanda:unit', $product->getData('dawanda_unit'));

        $unitsPerItem = $product->getData('dawanda_units_per_item');
        if ($unitsPerItem == '' OR (int)$unitsPerItem == 1) {
            $productPricing->addChild('dawanda:cents', (int) ($product->getPrice() * 100));
            // product pricing
            $productAvailabilty = $productXML->addChild('dawanda:availability', (int) $product->getStockItem()->getQty());
            $productBasepriceInformation->addChild('dawanda:milli_units_per_item', 1000);
        }
        else {
            $price = (int) ($product->getPrice() * 100);
            $price = $price * $unitsPerItem;
            $productPricing->addChild('dawanda:cents', (int) $price);
            // product pricing
            $productAvailabilty = $productXML->addChild('dawanda:availability', (int) ($product->getStockItem()->getQty() / $unitsPerItem));

            $productBasepriceInformation->addChild('dawanda:milli_units_per_item', $unitsPerItem * 1000);
        }


        $productPricing->addChild('dawanda:currency', 'EUR');


        return $productXML->asXML();
    }

    protected function _parseDescription($description) {

        $description = str_replace('<span>', '', $description);
        $description = str_replace('</span>', '', $description);
        $description = str_replace('<div>', '', $description);
        $description = str_replace('</div>', "\r", $description);
        $description = str_replace('<p>', '', $description);
        $description = str_replace('</p>', "\r", $description);
        $description = str_replace('<br>', "\r", $description);
        $description = str_replace('<br/>', "\r", $description);
        $description = str_replace('<br />', "\r", $description);
        $description = str_replace('<strong>', '*', $description);
        $description = str_replace('</strong>', '*', $description);
        $description = str_replace('<ul>', '', $description);
        $description = str_replace('</ul>', '', $description);
        $description = str_replace('<li>', '- ', $description);
        $description = str_replace('</li>', '', $description);

        $description .= "

Versandkosten von *3,90€* fallen nur *1x* pro Bestellung an. Alle Lieferungen erfolgen als DHL-Paket.

Weitere Infos und Angebote findet Ihr auf unserer Internetseite unter *http://www.stoffwerft.com*

Werdet stoffwerft-Fan auf *Facebook* und erfahrt so schnell von Aktionen und Neuigkeiten: *https://www.facebook.com/stoffwerft*";

        // remove duplicate whitespaces
        $description = strip_tags($description);
        #$description = preg_replace('/\s+/', ' ',$description);
        $description = html_entity_decode($description);
        $description = str_replace("\r", "&#13;\r", $description);

        for ($i = 1; $i<100;$i++) {
            $description = str_replace("&#13;\r&#13;\r", "&#13;\r", $description);
        }
        return $description;
    }

}