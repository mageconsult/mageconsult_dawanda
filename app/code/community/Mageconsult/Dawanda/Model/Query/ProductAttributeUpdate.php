<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Query_ProductAttributeUpdate extends Mageconsult_Dawanda_Model_Query_Abstract
{

    const URL = 'https://dawanda.com/seller_api/products/%s/attribute/availability/%s';

    protected function _construct()
    {
        return parent::_construct();

    }

    public function updateAvailabilty($sku, $qty) {

        $header = array(
            'X-Dawanda-Auth: ' . $this->getApiKey(),
            'Content-Type: text/xml'
        );

        $url = sprintf(self::URL, $sku, $qty);

        $result = parent::callAPI('PUT', $url , '', $header);

        #$result = new SimpleXMLElement($result, 0, false, self::DAWANDA_NAMESPACE, true);

        return $result;

    }

}