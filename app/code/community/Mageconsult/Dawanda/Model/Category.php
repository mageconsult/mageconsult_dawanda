<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Category extends Mageconsult_Dawanda_Model_Abstract
{

    protected $_collectionData;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/category');
    }


    public function updateDataFromDawanda()
    {

        $error = false;

        $language = Mage::getStoreConfig('dawanda/general/language');

        if (is_null($this->_collectionData)) {

            $collection = Mage::getModel('mageconsult_dawanda/category')->getCollection();

            $data = array();
            foreach ($collection as $id => $item) {
                $data[$item->getData('language') . '-' . $item->getData('category_id')] = null;

            }
            $this->_collectionData = $data;
        }

        $query      = Mage::getModel('mageconsult_dawanda/query_category');
        $categories = $query->query();

        $data = array();
        foreach ($categories->channel as $channel) {

            $attributes  = $channel->attributes();
            $level1_text = (string)$attributes->text;


            foreach ($channel->category_level_2 as $level2) {
                $attributes  = $level2->attributes();
                $level2_text = (string)$attributes->text;

                foreach ($level2->category_level_3 as $level3) {
                    $attributes  = $level3->attributes();
                    $level3_text = (string)$attributes->text;
                    $level3_id   = (int)$attributes->id;
                    $basePrice   = (string)$attributes->base_price === 'false';
                    $combiBuy    = (string)$attributes->combi_buy === false;

                    if (!array_key_exists($language . '-' . $level3_id, $this->_collectionData)) {

                        $category = new Varien_Object();


                        $category->setData('category_id', $level3_id);
                        $category->setData('level1', $level1_text);
                        $category->setData('level2', $level2_text);
                        $category->setData('level3', $level3_text);
                        $category->setData('language', 'de');
                        $category->setData('base_price', $basePrice);
                        $category->setData('combi_buy', $combiBuy);
                        $category->setData('created_at', now());
                        $category->setData('updated_at', now());

                        $data[] = $category->toArray();
                    }
                }
            }
        }

        // write data directly to table
        try {
            $this->getResource()->updateMultiple($data);
        } catch (Exception $e) {
            Mage::logException($e);
            $error = $e->getMessage();
        }

        return $error;
    }
}