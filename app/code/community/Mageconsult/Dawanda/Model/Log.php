<?php


class Mageconsult_Dawanda_Model_Log extends Mageconsult_Dawanda_Model_Abstract
{

    const OPEN = 0;
    const IN_PROGRESS = 1;
    CONST DONE = 9;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/log');
    }
}