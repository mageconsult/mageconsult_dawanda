<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 22:10
 */
class Mageconsult_Dawanda_Model_Dawanda extends Mageconsult_Dawanda_Model_Abstract
{

    /**
     * validate, if all data is synced
     */
    public function validate()
    {
        $error = array();

        $result = $this->_validateTable('mageconsult_dawanda/product');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/category');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/color');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/paymentmethod');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/returnpolicy');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/shippingprofile');
        if ($result !== false) {
            $error[] = $result;
        }

        $result = $this->_validateTable('mageconsult_dawanda/shopcategory');
        if ($result !== false) {
            $error[] = $result;
        }

        return $error;

    }

    protected function _validateCategory()
    {

        $error = false;

        $categorySize = Mage::getModel('mageconsult_dawanda/category')
            ->getCollection()
            ->getSize();

        if ($categorySize == 0) {
            $error = $this->helper('mageconsult_dawanda')->__('DaWanda Katerorien nicht vorhanden');
        }

        return $error;
    }

    protected function _validateTable($modelName)
    {

        $error = false;

        $collectionSize = Mage::getModel($modelName)
            ->getCollection()
            ->getSize();

        if ($collectionSize == 0) {
            $error = Mage::helper('mageconsult_dawanda')->__($modelName . ' ohne Inhalt');
        }

        return $error;
    }

}