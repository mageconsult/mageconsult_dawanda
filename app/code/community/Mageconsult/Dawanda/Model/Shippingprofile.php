<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 22:30
 */ 
class Mageconsult_Dawanda_Model_Shippingprofile extends Mageconsult_Dawanda_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/shippingprofile');
    }

    public function updateDataFromDawanda()
    {

        $query  = Mage::getModel('mageconsult_dawanda/query_shippingprofile');
        $shippingprofiles = $query->query();

        foreach ($shippingprofiles as $shippingprofile) {

            $attributes = $shippingprofile->attributes();
            $id = (int) $attributes->id;
            $name = (string) $attributes->name;

            $paymentmethodModel = Mage::getModel('mageconsult_dawanda/shippingprofile');
            $paymentmethodModel->load($id, 'shippingprofile_id');

            $paymentmethodModel->setData('shippingprofile_id', $id);
            $paymentmethodModel->setData('language', 'de');
            $paymentmethodModel->setData('name', $name);
            $paymentmethodModel->setData('created_at', now());
            $paymentmethodModel->setData('updated_at', now());

            try {
                $paymentmethodModel->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }
}