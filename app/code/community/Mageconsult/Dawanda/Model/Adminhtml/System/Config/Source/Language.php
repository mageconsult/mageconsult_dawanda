<?php

class Mageconsult_Dawanda_Model_Adminhtml_System_Config_Source_Language
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'de', 'label' => Mage::helper('mageconsult_dawanda')->__('deutsch')),
            array('value' => 'en', 'label' => Mage::helper('mageconsult_dawanda')->__('englisch')),
            array('value' => 'es', 'label' => Mage::helper('mageconsult_dawanda')->__('spanisch')),
            array('value' => 'pl', 'label' => Mage::helper('mageconsult_dawanda')->__('polnisch')),
            array('value' => 'fr', 'label' => Mage::helper('mageconsult_dawanda')->__('französisch')),
            array('value' => 'nl', 'label' => Mage::helper('mageconsult_dawanda')->__('niederländisch')),
            array('value' => 'it', 'label' => Mage::helper('mageconsult_dawanda')->__('italienisch')),
        );
    }
}

