<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 23:03
 */
class Mageconsult_Dawanda_Model_Product extends Mageconsult_Dawanda_Model_Abstract
{

    const SYNC_DISABLED = 0;
    const SYNC_STOCKDATA = 1;
    const SYNC_FULLDATA = 2;

    protected $_collection;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/product');
    }

    public function updateDataFromDawanda()
    {
        /* @var $resource Mage_Core_Model_Resource */
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        $tableName = $resource->getTableName('mageconsult_dawanda/product');
        $write->query('truncate table ' . $tableName);

        $collection = Mage::getModel('mageconsult_dawanda/product')->getCollection();

        $dawandaData = array();
        foreach ($collection as $id => $item) {
            $dawandaData[$item->getData('product_id')] = null;
        }

        /* @var $query Mageconsult_Dawanda_Model_Query_Product */
        $query    = Mage::getModel('mageconsult_dawanda/query_product');
        $products = $query->query();

        foreach ($products->product as $product) {

            $dawandaData = array();

            $attributes = $product->attributes();
            $productId  = (int)$attributes->id;

            $dawandaData['product_id']          = $productId;
            $dawandaData['auto_renew_enabled']  = (bool)$attributes->auto_renew_enabled;
            $dawandaData['category_id']         = (int)$attributes->category_id;
            $dawandaData['availability']        = (int)$product->availability;
            $dawandaData['color1']              = (string)$attributes->color_1;
            $dawandaData['color2']              = (string)$attributes->color_2;
            $dawandaData['mailable_in']         = (int)$attributes->mailable_in;
            $dawandaData['return_policy_id']    = (int)$attributes->return_policy_id;
            $dawandaData['shipping_profile_id'] = (int)$attributes->shipping_profile_id;
            $dawandaData['shop_category_id']    = (int)$attributes->shop_category_id;
            $dawandaData['sku']                 = (string)$attributes->sku;
            $dawandaData['status']              = (string)$attributes->status;
            $dawandaData['status']              = (string)$attributes->status;
            $dawandaData['typology']            = (int)$attributes->typology;
            $dawandaData['valid']               = (bool)$attributes->valid;
            $dawandaData['valid_to']            = (string)$attributes->valid_to;

            $dawandaData['cents']                = (string)$product->pricing_information->cents;
            $dawandaData['currency']             = (string)$product->pricing_information->currency;
            $dawandaData['unit']                 = (string)$product->pricing_information->base_price_information->unit;
            $dawandaData['milli_units_per_item'] = (string)$product->pricing_information->base_price_information->milli_units_per_item;

            $dawandaData['title']            = (string)$product->product_texts->product_text->title;
            $dawandaData['description']      = (string)$product->product_texts->product_text->description;
            $dawandaData['tags']             = (string)$product->product_texts->product_text->tags;
            $dawandaData['basic_attributes'] = (string)$product->product_texts->product_text->basic_attributes;


            $i = 0;

            $dawandaData['image1'] = '';
            $dawandaData['image2'] = '';
            $dawandaData['image3'] = '';
            $dawandaData['image4'] = '';
            foreach ($product->images->image as $image) {
                $i++;
                $attributes                = $image->attributes();
                $dawandaData['image' . $i] = (string)$attributes->uri;
                if ($i > 4) {
                    break;
                }

            }

            if (!array_key_exists($productId, $dawandaData)) {
                $data[] = $dawandaData;
            }
        }

        // write data directly to table
        try {
            $this->getResource()->updateMultiple($data);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function updateProductsWithDawanda()
    {

        $storeId    = 0;
        $collection = $this->_getCollection();

        foreach ($collection as $dawandaProduct) {

            // TODO: replace with faster updateAttribute()

            /** @var $product Mage_Catalog_Model_Product */
            $product   = Mage::getModel('catalog/product');
            $productId = $product->getIdBySku($dawandaProduct->getData('sku'));
            $product->load($productId);
            if ($product->getId()) {
                if ((int) $product->getData('dawanda_enable_sync') == self::SYNC_DISABLED) {
                    $product->addAttributeUpdate('dawanda_enable_sync', self::SYNC_STOCKDATA, $storeId);
                }
                $product->addAttributeUpdate('dawanda_tags_as_text', $dawandaProduct->getData('tags'), $storeId);
                $product->addAttributeUpdate('dawanda_basic_attributes', $dawandaProduct->getData('basic_attributes'), $storeId);
                $product->addAttributeUpdate('dawanda_category1', $dawandaProduct->getData('category_id'), $storeId);
                $product->addAttributeUpdate('dawanda_category2', $dawandaProduct->getData('additional_category_id'), $storeId);
                $product->addAttributeUpdate('dawanda_shopcategory', $dawandaProduct->getData('shop_category_id'), $storeId);
                $product->addAttributeUpdate('dawanda_color1', $dawandaProduct->getData('color1'), $storeId);
                $product->addAttributeUpdate('dawanda_color2', $dawandaProduct->getData('color2'), $storeId);
                $product->addAttributeUpdate('dawanda_unit', $dawandaProduct->getData('unit'), $storeId);

                $milliUnitPerItem = $dawandaProduct->getData('milli_units_per_item');
                $milliUnitPerItem = (float) $milliUnitPerItem / 1000;
                $product->addAttributeUpdate('dawanda_units_per_item', $milliUnitPerItem, $storeId);

                $product->addAttributeUpdate('dawanda_mailable_in_option', $dawandaProduct->getData('sku'), $storeId);
                $product->addAttributeUpdate('dawanda_typology', $dawandaProduct->getData('typology'), $storeId);
                $product->addAttributeUpdate('dawanda_shippingprofile', $dawandaProduct->getData('shipping_profile_id'), $storeId);
                $product->addAttributeUpdate('dawanda_returnpolicy', $dawandaProduct->getData('return_policy_id'), $storeId);
                $product->addAttributeUpdate('dawanda_auto_renew_enabled', $dawandaProduct->getData('auto_renew_enabled'), $storeId);
                #$product->save();
            }
        }
    }

    protected function _getCollection()
    {
        if (is_null($this->_collection)) {
            $this->_collection = Mage::getModel('mageconsult_dawanda/product')->getCollection();
        }

        return $this->_collection;
    }

}