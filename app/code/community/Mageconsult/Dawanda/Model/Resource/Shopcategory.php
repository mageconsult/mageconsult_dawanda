<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 20:55
 */
class Mageconsult_Dawanda_Model_Resource_Shopcategory extends Mageconsult_Dawanda_Model_Resource_Abstract
{
    protected $_tableName;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/shopcategory', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/shopcategory');
    }

}