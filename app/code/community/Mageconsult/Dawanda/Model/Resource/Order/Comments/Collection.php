<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 18.02.17
 * Time: 17:35
 */ 
class Mageconsult_Dawanda_Model_Resource_Order_Comments_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/order_comments');
    }

}