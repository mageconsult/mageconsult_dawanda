<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 21:36
 */ 
class Mageconsult_Dawanda_Model_Resource_Paymentmethod_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/paymentmethod');
    }

}