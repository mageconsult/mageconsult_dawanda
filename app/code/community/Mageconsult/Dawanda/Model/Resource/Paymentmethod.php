<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 21:36
 */ 
class Mageconsult_Dawanda_Model_Resource_Paymentmethod extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/paymentmethod', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/paymentmethod');
    }



}