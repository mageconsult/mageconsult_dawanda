<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 18.02.17
 * Time: 17:34
 */ 
class Mageconsult_Dawanda_Model_Resource_Order extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/order', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/order');
    }

}