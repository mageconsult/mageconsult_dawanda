<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */ 
class Mageconsult_Dawanda_Model_Resource_Category_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/category');
    }

}