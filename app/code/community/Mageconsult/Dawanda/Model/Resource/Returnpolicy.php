<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 05.02.16
 * Time: 00:21
 */ 
class Mageconsult_Dawanda_Model_Resource_Returnpolicy extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/returnpolicy', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/returnpolicy');
    }

}