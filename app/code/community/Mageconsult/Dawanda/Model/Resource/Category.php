<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 18:37
 */
class Mageconsult_Dawanda_Model_Resource_Category extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/category', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/category');
    }

}