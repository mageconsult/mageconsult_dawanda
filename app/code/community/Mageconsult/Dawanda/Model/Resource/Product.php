<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 23:03
 */ 
class Mageconsult_Dawanda_Model_Resource_Product extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/product', 'entity_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/product');
    }


}