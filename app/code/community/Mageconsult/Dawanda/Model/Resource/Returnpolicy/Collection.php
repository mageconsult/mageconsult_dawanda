<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 05.02.16
 * Time: 00:21
 */ 
class Mageconsult_Dawanda_Model_Resource_Returnpolicy_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/returnpolicy');
    }

}