<?php

class Mageconsult_Dawanda_Model_Resource_Queue extends Mageconsult_Dawanda_Model_Resource_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/queue', 'queue_id');
        $this->_tableName = $this->getTable('mageconsult_dawanda/queue');
    }
}