<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:20
 */ 
class Mageconsult_Dawanda_Model_Resource_Sync extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/sync', 'entity_id');
    }

}