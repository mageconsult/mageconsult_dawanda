<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 20:55
 */ 
class Mageconsult_Dawanda_Model_Resource_Abstract extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_tableName;

    protected function _construct()
    {
        parent::_construct();
    }


    public function updateMultiple($data) {

        $write = $this->_getWriteAdapter();

        if ($data) {
            $write->insertMultiple($this->_tableName, $data);
        }
    }

}