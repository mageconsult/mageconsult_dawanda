<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:20
 */ 
class Mageconsult_Dawanda_Model_Resource_Sync_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/sync');
    }

}