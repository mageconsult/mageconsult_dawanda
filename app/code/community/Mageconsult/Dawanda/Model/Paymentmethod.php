<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 21:36
 */ 
class Mageconsult_Dawanda_Model_Paymentmethod extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/paymentmethod');
    }

    public function updateDataFromDawanda()
    {

        $query  = Mage::getModel('mageconsult_dawanda/query_paymentmethod');
        $paymentmethods = $query->query();

        foreach ($paymentmethods as $paymentmethod) {

            $attributes = $paymentmethod->attributes();
            $id = (int) $attributes->id;
            $title = (string) $attributes->title;

            $title = strip_tags($title);

            $paymentmethodModel = Mage::getModel('mageconsult_dawanda/paymentmethod');
            $paymentmethodModel->load($id, 'paymentmethod_id');

            $paymentmethodModel->setData('paymentmethod_id', $id);
            $paymentmethodModel->setData('language', 'de');
            $paymentmethodModel->setData('title', $title);
            $paymentmethodModel->setData('created_at', now());
            $paymentmethodModel->setData('updated_at', now());

            try {
                $paymentmethodModel->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

}