<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 20:55
 */
class Mageconsult_Dawanda_Model_Shopcategory extends Mageconsult_Dawanda_Model_Abstract
{

    protected $_collectionData;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/shopcategory');
    }

    public function updateDataFromDawanda()
    {
        $language = Mage::getStoreConfig('dawanda/general/language');

        if (is_null($this->_collectionData)) {

            $collection = Mage::getModel('mageconsult_dawanda/shopcategory')->getCollection();

            $data = array();
            foreach ($collection as $id => $item) {
                $data[$item->getData('language') . '-' . $item->getData('shopcategory_id')] = $item;

            }
            $this->_collectionData = $data;
        }

        $query  = Mage::getModel('mageconsult_dawanda/query_shopcategory');
        $shopcategories = $query->query();

        $data = array();
        foreach ($shopcategories as $shopCategory) {

            $attributes = $shopCategory->attributes();
            $id = (int) $attributes->id;
            $name = (string) $attributes->name;
            $position = (int) $attributes->position;

            if (!array_key_exists($language . '-' . $id, $this->_collectionData)) {

                $shopcategory = new Varien_Object();

                $shopcategory->setData('shopcategory_id', $id);
                $shopcategory->setData('language', 'de');
                $shopcategory->setData('name', $name);
                $shopcategory->setData('position', $position);
                $shopcategory->setData('created_at', now());
                $shopcategory->setData('updated_at', now());

                $data[] = $shopcategory->toArray();
            }
        }

        // write data directly to table
        try {
            $this->getResource()->updateMultiple($data);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}