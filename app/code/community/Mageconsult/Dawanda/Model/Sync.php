<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:20
 */
class Mageconsult_Dawanda_Model_Sync extends Mageconsult_Dawanda_Model_Abstract
{

    const STATUS_DISABLED = 0;
    const STATUS_NEW = 1;
    const STATUS_STOCKUDDATE = 2;
    const STATUS_PRODUCTUPDATE = 3;

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/sync');
    }


    public function generate()
    {

        /* @var $resource Mage_Core_Model_Resource */
        $resource  = Mage::getSingleton('core/resource');
        $write     = $resource->getConnection('core_write');
        $tableName = $resource->getTableName('mageconsult_dawanda/sync');
        $write->query('truncate table ' . $tableName);

        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToFilter('dawanda_enable_sync',
            array(
                Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA,
                Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA)
        );
        $collection->addAttributeToSelect(array(
            'name',
            'status',
            'visibility',
            'dawanda_enable_sync',
            'dawanda_tags_as_text',
            'dawanda_basic_attributes',
            'dawanda_category1',
            'dawanda_category2',
            'dawanda_shopcategory',
            'dawanda_color1',
            'dawanda_color2',
            'dawanda_unit',
            'dawanda_units_per_item',
            'dawanda_mailable_in_option',
            'dawanda_typology',
            'dawanda_shippingprofile',
            'dawanda_returnpolicy',
            'dawanda_auto_renew_enabled',
        ));
        $collection->setFlag('require_stock_items', true);

        /** @var $product Mage_Catalog_Model_Product */
        foreach ($collection as $product) {

            /** @var $dawandaProduct Mageconsult_Dawanda_Model_Product */
            $dawandaProduct = Mage::getModel('mageconsult_dawanda/product');
            $dawandaProduct->load($product->getSku(), 'sku');

            /** @var $syncProduct Mageconsult_Dawanda_Model_Sync */
            $syncProduct = Mage::getModel('mageconsult_dawanda/sync');
            $syncProduct->setData('product_id', $product->getId());
            $syncProduct->setData('sku', $product->getSku());
            $syncProduct->setData('title', $product->getData('name'));

            /** @var $stockItem Mage_CatalogInventory_Model_Stock_Item */
            $stockItem    = $product->getStockItem();
            $isDecimalQty = (bool)$stockItem->getData('is_qty_decimal');
            if ($isDecimalQty) {
                $p1 = intval((float)$stockItem->getQty() / (float)$stockItem->getData('min_sale_qty'));
            } else {
                $p1 = (int)$stockItem->getQty();
            }

            if ($product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                $p1 = 0;
            }

            $p2 = $dawandaProduct->getData('availability');
            $syncProduct->setData('magento_data', $p1);
            $syncProduct->setData('dawanda_data', $p2);

            if ($product->getData('dawanda_enable_sync') == Mageconsult_Dawanda_Model_Product::SYNC_STOCKDATA && $p1 <> $p2) {
                $syncProduct->setData('status', self::STATUS_STOCKUDDATE);
            } elseif ($product->getData('dawanda_enable_sync') == Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA) {
                $syncProduct->setData('status', self::STATUS_PRODUCTUPDATE);
            } elseif ($product->getData('dawanda_enable_sync') == Mageconsult_Dawanda_Model_Product::SYNC_FULLDATA && !$dawandaProduct->getId()) {
                $syncProduct->setData('status', self::STATUS_NEW);
            }

            if ($syncProduct->getStatus()) {
                $syncProduct->save();
            }
        }
    }


}