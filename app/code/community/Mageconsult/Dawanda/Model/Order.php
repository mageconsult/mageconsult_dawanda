<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 18.02.17
 * Time: 17:34
 */
class Mageconsult_Dawanda_Model_Order extends Mageconsult_Dawanda_Model_Abstract
{

    const STATE_MARKED_AS_PAID = 'marked_as_paid';
    const STATE_CONFIRMED = 'confirmed';
    const STATE_CLOSED = 'closed';

    protected function _construct()
    {
        $this->_init('mageconsult_dawanda/order');
    }

    public function updateDataFromDawanda()
    {

        $data = array();

        $collection = Mage::getModel('mageconsult_dawanda/order')->getCollection();

        $dawandaData = array();
        foreach ($collection as $id => $item) {
            $dawandaData[$item->getData('product_id')] = null;
        }

        /* @var $query Mageconsult_Dawanda_Model_Query_Order */
        $query = Mage::getModel('mageconsult_dawanda/query_order');
        $orders = $query->query();

        foreach ($orders->order as $order) {

            $dawandaData = array();

            $attributes = $order->attributes();
            $orderId = (int)$attributes->id;

            $dawandaData['order_id'] = $orderId;

            $dawandaData['adjustment_reason'] = (int)$attributes->adjustment_reason;
            $dawandaData['adjustment_cents'] = (int)$attributes->adjustment_cents;
            $dawandaData['cents_total'] = (int)$attributes->cents_total;
            $dawandaData['cents_total_shipping'] = (int)$attributes->cents_total_shipping;
            $dawandaData['currency'] = (string)$attributes->currency;
            $dawandaData['gender'] = (string)$attributes->gender;
            $dawandaData['username'] = (string)$attributes->username;
            $dawandaData['user_id'] = (int)$attributes->user_id;
            $dawandaData['dawanda_created_at'] = (string)$attributes->created_at;
            $dawandaData['dawanda_updated_at'] = (string)$attributes->updated_at;
            $dawandaData['created_at'] = now();
            $dawandaData['updated_at'] = now();

            $invoiceAddress = $order->invoice_address->attributes();
            $dawandaData['invoice_street'] = (string)$invoiceAddress->street;
            $dawandaData['invoice_city'] = (string)$invoiceAddress->city;
            $dawandaData['invoice_country'] = (string)$invoiceAddress->country;
            $dawandaData['invoice_line2'] = (string)$invoiceAddress->line2;
            $dawandaData['invoice_line3'] = (string)$invoiceAddress->line3;
            $dawandaData['invoice_zip'] = (string)$invoiceAddress->zip;
            $dawandaData['invoice_email'] = (string)$invoiceAddress->email;
            $dawandaData['invoice_firstname'] = (string)$invoiceAddress->firstname;
            $dawandaData['invoice_lastname'] = (string)$invoiceAddress->lastname;
            $dawandaData['invoice_company'] = (string)$invoiceAddress->company;

            $shippingAddress = $order->shipping_address->attributes();
            $dawandaData['shipping_street'] = (string)$shippingAddress->street;
            $dawandaData['shipping_city'] = (string)$shippingAddress->city;
            $dawandaData['shipping_country'] = (string)$shippingAddress->country;
            $dawandaData['shipping_line2'] = (string)$shippingAddress->line2;
            $dawandaData['shipping_line3'] = (string)$shippingAddress->line3;
            $dawandaData['shipping_zip'] = (string)$shippingAddress->zip;
            $dawandaData['shipping_email'] = (string)$shippingAddress->email;
            $dawandaData['shipping_firstname'] = (string)$shippingAddress->firstname;
            $dawandaData['shipping_lastname'] = (string)$shippingAddress->lastname;
            $dawandaData['shipping_company'] = (string)$shippingAddress->company;

            $orderStatus = $order->order_status->attributes();
            $dawandaData['dawanda_sent_at'] = (string)$orderStatus->sent_at;
            $dawandaData['dawanda_marked_as_paid_at'] = (string)$orderStatus->marked_as_paid_at;
            $dawandaData['dawanda_confirmed_at'] = (string)$orderStatus->confirmed_at;
            $dawandaData['dawanda_status'] = (string)$orderStatus->status;

            $paymentInfo = $order->payment_info->attributes();
            $dawandaData['paid_currency'] = (string)$paymentInfo->paid_currency;
            $dawandaData['payment_method_id'] = (string)$paymentInfo->payment_method_id;
            $dawandaData['paid_cents'] = (string)$paymentInfo->paid_cents;
            $dawandaData['mangopay_status'] = (string)$paymentInfo->mangopay_status;
            $dawandaData['mangopay_timestamp'] = (string)$paymentInfo->mangopay_timestamp;

            if (!array_key_exists($orderId, $dawandaData)) {
                $data[] = $dawandaData;
            }
        }

        // write data directly to table
        try {
            $this->getResource()->updateMultiple($data);
        } catch (Exception $e) {
            Mage::logException($e);
        }

    }

    public function getOptionArray() {

        return array(
            self::STATE_CLOSED => self::STATE_CLOSED,
            self::STATE_MARKED_AS_PAID => self::STATE_MARKED_AS_PAID,
            self::STATE_CONFIRMED => self::STATE_CONFIRMED,
        );

    }
}