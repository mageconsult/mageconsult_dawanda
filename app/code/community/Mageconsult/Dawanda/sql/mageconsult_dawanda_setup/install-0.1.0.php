<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 17:33
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$connection = $installer->getConnection();

// table names
$productTableName         = 'mageconsult_dawanda/product';
$categoryTableName        = 'mageconsult_dawanda/category';
$shopcategoryTableName    = 'mageconsult_dawanda/shopcategory';
$paymentmethodTableName   = 'mageconsult_dawanda/paymentmethod';
$colorTableName           = 'mageconsult_dawanda/color';
$shippingprofileTableName = 'mageconsult_dawanda/shippingprofile';
$returnpolicyTableName    = 'mageconsult_dawanda/returnpolicy';
$syncTableName            = 'mageconsult_dawanda/sync';

// drop table if exists
$productTable         = $connection->dropTable($installer->getTable($productTableName));
$categoryTable        = $connection->dropTable($installer->getTable($categoryTableName));
$shopcategoryTable    = $connection->dropTable($installer->getTable($shopcategoryTableName));
$paymentmethodTable   = $connection->dropTable($installer->getTable($paymentmethodTableName));
$colorTable           = $connection->dropTable($installer->getTable($colorTableName));
$shippingprofileTable = $connection->dropTable($installer->getTable($shippingprofileTableName));
$returnpolicyTable    = $connection->dropTable($installer->getTable($returnpolicyTableName));
$syncTable            = $connection->dropTable($installer->getTable($syncTableName));

// generate table
$productTable         = $connection->newTable($installer->getTable($productTableName));
$categoryTable        = $connection->newTable($installer->getTable($categoryTableName));
$shopcategoryTable    = $connection->newTable($installer->getTable($shopcategoryTableName));
$paymentmethodTable   = $connection->newTable($installer->getTable($paymentmethodTableName));
$colorTable           = $connection->newTable($installer->getTable($colorTableName));
$shippingprofileTable = $connection->newTable($installer->getTable($shippingprofileTableName));
$returnpolicyTable    = $connection->newTable($installer->getTable($returnpolicyTableName));
$syncTable            = $connection->newTable($installer->getTable($syncTableName));


/**
 * product table
 */
$productTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false
    ))
    ->addColumn('availability', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('shop_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('additional_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('color1', Varien_Db_Ddl_Table::TYPE_VARCHAR, 7, array(
        'nullable' => false
    ))
    ->addColumn('color2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 7, array(
        'nullable' => false
    ))
    ->addColumn('shipping_profile_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('return_policy_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('auto_renew_enabled', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('valid', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('valid_to', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('typology', Varien_Db_Ddl_Table::TYPE_INTEGER, 2, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('mailable_in', Varien_Db_Ddl_Table::TYPE_INTEGER, 2, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('publish', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 6, array(
        'nullable' => false
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('customization_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('material_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('manufacturing_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('size_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('tags', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false
    ))
    ->addColumn('basic_attributes', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('image1', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image3', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image4', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image_update_required', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => false
    ))
    ->addColumn('cents', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('currency', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('unit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('base_price_unit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('milli_units_per_item', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('additional_attribute', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($productTableName),
            array('product_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('product_id')
    );

try {
    $connection->createTable($productTable);
} catch (Exception $e) {
    Mage::logException($e);
}


/**
 * category table
 */
$categoryTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('level1', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('level2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('level3', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('base_price', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => false
    ))
    ->addColumn('combi_buy', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($categoryTableName),
            array('category_id', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('category_id', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($categoryTableName),
            array('level1'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('level1')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($categoryTableName),
            array('level2'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('level2')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($categoryTableName),
            array('level3'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('level3')
    );

try {
    $connection->createTable($categoryTable);
} catch (Exception $e) {
    Mage::logException($e);
}


/**
 * shopcategory table
 */
$shopcategoryTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('shopcategory_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 5, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($shopcategoryTableName),
            array('shopcategory_id', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('shopcategory_id', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($shopcategoryTableName),
            array('name'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('name')
    );

try {
    $connection->createTable($shopcategoryTable);
} catch (Exception $e) {
    Mage::logException($e);
}


/**
 * paymentmethod table
 */
$paymentmethodTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('paymentmethod_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($paymentmethodTableName),
            array('paymentmethod_id', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('paymentmethod_id', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($paymentmethodTableName),
            array('title'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('title')
    );

try {
    $connection->createTable($paymentmethodTable);
} catch (Exception $e) {
    Mage::logException($e);
}


/**
 * color table
 */
$colorTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('hex_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 6, array(
        'nullable' => false
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($colorTableName),
            array('hex_code', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('hex_code', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($colorTableName),
            array('name'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('name')
    );

try {
    $connection->createTable($colorTable);
} catch (Exception $e) {
    Mage::logException($e);
}


/**
 * shippingprofile table
 */
$shippingprofileTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('shippingprofile_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($shippingprofileTableName),
            array('shippingprofile_id', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('shippingprofile_id', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($shippingprofileTableName),
            array('name'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('name')
    );

try {
    $connection->createTable($shippingprofileTable);
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();


/**
 * returnpolicy table
 */
$returnpolicyTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('language', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2, array(
        'nullable' => false
    ))
    ->addColumn('returnpolicy_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('text', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($returnpolicyTableName),
            array('returnpolicy_id', 'language'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('returnpolicy_id', 'language')
    )
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($returnpolicyTableName),
            array('title'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('title')
    );

try {
    $connection->createTable($returnpolicyTable);
} catch (Exception $e) {
    Mage::logException($e);
}

/**
 * sync table - holds data to sync between magento <-> dawanda
 */
$syncTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'       => true,
        'unsigned'       => true,
        'nullable'       => false,
        'primary'        => true,
        'auto_increment' => true
    ))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false
    ))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('shop_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('additional_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'unsigned' => true,
        'nullable' => false
    ))
    ->addColumn('color1', Varien_Db_Ddl_Table::TYPE_VARCHAR, 7, array(
        'nullable' => false
    ))
    ->addColumn('color2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 7, array(
        'nullable' => false
    ))
    ->addColumn('shipping_profile_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('return_policy_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('auto_renew_enabled', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('valid', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('valid_to', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('typology', Varien_Db_Ddl_Table::TYPE_INTEGER, 2, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('mailable_in', Varien_Db_Ddl_Table::TYPE_INTEGER, 2, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('publish', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => true
    ))
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1, array(
        'nullable' => false
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false
    ))
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('customization_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('material_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('manufacturing_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('size_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('tags', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false
    ))
    ->addColumn('basic_attributes', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('image1', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image3', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image4', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false
    ))
    ->addColumn('image_update_required', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'unsigned' => true,
        'default'  => false
    ))
    ->addColumn('cents', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('currency', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('unit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('base_price_unit', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('milli_units_per_item', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('additional_attribute', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))

    ->addColumn('magento_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('magento_hash', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable' => false
    ))
    ->addColumn('dawanda_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('dawanda_hash', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable' => false
    ))

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($syncTableName),
            array('product_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('product_id')
    );

try {
    $connection->createTable($syncTable);
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();


/******************************
 * Adding Attributes to product
 *****************************/

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$attributeGroupName = "DaWanda";

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

// add attribute group to all sets


$attributeSetIds = array();

$attributeSetCollection = Mage::getModel('eav/entity_attribute_set')
    ->getResourceCollection()
    ->addFilter('entity_type_id', $entityTypeId);

foreach ($attributeSetCollection as $id => $attributeSet) {
    // fetch attribute set
    $attributeSetIds[] = $id;

    // save new group to set
    $modelGroup = Mage::getModel('eav/entity_attribute_group');
    $modelGroup->setAttributeGroupName($attributeGroupName)
        ->setAttributeSetId($attributeSet->getId())
        ->setSortOrder(100);
    try {
        $modelGroup->save();
    } catch (Exception $e) {
        Mage::logException($e);
    }
}

$dawandaAttributes = array();

$dawandaAttributes['dawanda_enable_sync'] =
    array(
        'label'                   => 'Sync with DaWanda',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_synctype',
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 10
    );

$dawandaAttributes['dawanda_tags_as_text'] =
    array(
        'label'                   => 'Dawanda Tags (comma seperated)',
        'type'                    => 'varchar',
        'input'                   => 'text',
        'group'                   => $attributeGroupName,
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 20
    );


$dawandaAttributes['dawanda_basic_attributes'] =
    array(
        'label'                   => 'Dawanda Basic Attributes',
        'type'                    => 'varchar',
        'input'                   => 'text',
        'group'                   => $attributeGroupName,
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 30
    );


$dawandaAttributes['dawanda_category1'] =
    array(
        'label'                   => 'Dawanda Category',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_category',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 40
    );

$dawandaAttributes['dawanda_category2'] =
    array(
        'label'                   => 'Dawanda Optional Category',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_category',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 50
    );

$dawandaAttributes['dawanda_shopcategory'] =
    array(
        'label'                   => 'Dawanda Own Shop Category',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_shopcategory',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 60
    );


$dawandaAttributes['dawanda_color1'] =
    array(
        'label'                   => 'Dawanda Color 1',
        'type'                    => 'varchar',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_color',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 70
    );


$dawandaAttributes['dawanda_color2'] =
    array(
        'label'                   => 'Dawanda Color 2',
        'type'                    => 'varchar',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_color',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 80
    );

$dawandaAttributes['dawanda_unit'] =
    array(
        'label'                   => 'Dawanda Unit',
        'type'                    => 'varchar',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_unit',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 90
    );


$dawandaAttributes['dawanda_units_per_item'] =
    array(
        'label'                   => 'Dawanda Units per Item',
        'type'                    => 'varchar',
        'input'                   => 'text',
        'group'                   => $attributeGroupName,
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 100
    );


$dawandaAttributes['dawanda_mailable_in_option'] =
    array(
        'label'                   => 'Dawanda Lieferzeit in Tagen',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_mailable',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 110
    );


$dawandaAttributes['dawanda_typology'] =
    array(
        'label'                   => 'Dawanda Typology',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_typology',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 120
    );


$dawandaAttributes['dawanda_shippingprofile'] =
    array(
        'label'                   => 'Dawanda Shipping Profile',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_shippingprofile',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 130
    );

$dawandaAttributes['dawanda_returnpolicy'] =
    array(
        'label'                   => 'Dawanda Return Policy',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_returnpolicy',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 140
    );

/*
$dawandaAttributes['dawanda_paymentmethod'] =
    array(
        'label'                   => 'Dawanda Payment Method',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'mageconsult_dawanda/catalog_product_attribute_source_paymentmethod',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 150
    );
*/

$dawandaAttributes['dawanda_auto_renew_enabled'] =
    array(
        'label'                   => 'Dawanda Auto Renew Enabled',
        'type'                    => 'int',
        'input'                   => 'select',
        'group'                   => $attributeGroupName,
        'source'                  => 'eav/entity_attribute_source_boolean',
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'required'                => false,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'visible_on_front'        => false,
        'unique'                  => false,
        'used_in_product_listing' => false,
        'position'                => 160
    );


// iterate all attributes
foreach ($dawandaAttributes as $attributeCode => $data) {

    //remove attribute
    $installer->removeAttribute('catalog_product', $attributeCode);

    // Create attribute:
    $installer->addAttribute('catalog_product', $attributeCode, $data);

    // Add the attribute to the proper sets/groups:
    $attributeId = $installer->getAttributeId($entityTypeId, $attributeCode);
    foreach ($attributeSetIds as $attributeSetId) {
        $attributeGroup   = $installer->getAttributeGroup($entityTypeId, $attributeSetId, $attributeGroupName);
        $attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $attributeGroup['attribute_group_id']);
        $installer->addAttributeToGroup('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
    }
}

$installer->endSetup();
