<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 17:33
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

// table names
$logTableName = 'mageconsult_dawanda/log';
$queueTableName = 'mageconsult_dawanda/queue';
$orderTableName = 'mageconsult_dawanda/order';
$orderItemsTableName = 'mageconsult_dawanda/order_items';
$orderCommentsTableName = 'mageconsult_dawanda/order_comments';

// drop table if exists
$logTable = $connection->dropTable($installer->getTable($logTableName));
$queueTable = $connection->dropTable($installer->getTable($queueTableName));
$orderTable = $connection->dropTable($installer->getTable($orderTableName));
$orderItemsTable = $connection->dropTable($installer->getTable($orderItemsTableName));
$orderCommentsTable = $connection->dropTable($installer->getTable($orderCommentsTableName));

// generate table
$logTable = $connection->newTable($installer->getTable($logTableName));
$queueTable = $connection->newTable($installer->getTable($queueTableName));
$orderTable = $connection->newTable($installer->getTable($orderTableName));
$orderItemsTable = $connection->newTable($installer->getTable($orderItemsTableName));
$orderCommentsTable = $connection->newTable($installer->getTable($orderCommentsTableName));

/**
 * log table
 */
$logTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('request', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('response', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('errortext', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ));

try {
    $connection->createTable($logTable);
} catch (Exception $e) {
    Mage::logException($e);
}

/**
 * queue table
 */
$queueTable
    ->addColumn('queue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false
    ))
    ->addColumn('state', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
    ))
    ->addColumn('executed_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
    ));

try {
    $connection->createTable($queueTable);
} catch (Exception $e) {
    Mage::logException($e);
}

/**
 * order table
 */
$orderTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('adjustment_reason', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))
    ->addColumn('gender', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false
    ))
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('cents_total', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('username', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable' => false,
    ))
    ->addColumn('currency', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('cents_total_shipping', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('adjustment_cents', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('payment_method_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 3, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('paid_currency', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('paid_cents', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('mangopay_status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('mangopay_timestamp', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))

    ->addColumn('invoice_street', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_city', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_country', Varien_Db_Ddl_Table::TYPE_VARCHAR, 3, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_line2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_line3', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_zip', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_firstname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_lastname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('invoice_company', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))

    ->addColumn('shipping_street', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_city', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_country', Varien_Db_Ddl_Table::TYPE_VARCHAR, 3, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_line2', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_line3', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_zip', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_firstname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_lastname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))
    ->addColumn('shipping_company', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ))

    ->addColumn('dawanda_sent_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('dawanda_marked_as_paid_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('dawanda_confirmed_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('dawanda_created_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('dawanda_updated_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('dawanda_status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable' => false,
    ))

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))

    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($orderTableName),
            array('order_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        ),
        array('order_id')
    );

try {
    $connection->createTable($orderTable);
} catch (Exception $e) {
    Mage::logException($e);
}

/**
 * order-items table
 */
$orderItemsTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true,
    ))
    ->addColumn('cents', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('cents_shipping_domestic', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('cents_shipping_international', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('quantity', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('product_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 60, array(
        'nullable' => false
    ))
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable' => false
    ))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))

    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($orderItemsTableName),
            array('order_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        ),
        array('order_id')
    );

try {
    $connection->createTable($orderItemsTable);
} catch (Exception $e) {
    Mage::logException($e);
}

/**
 * order-comments table
 */
$orderCommentsTable
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'auto_increment' => true
    ))
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 15, array(
        'nullable' => false,
        'unsigned' => true,
    ))
    ->addColumn('dawanda_created_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable' => false,
    ))
    ->addColumn('username', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable' => false,
    ))
    ->addColumn('body', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false
    ))

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ))

    ->addIndex(
        $installer->getIdxName(
            $installer->getTable($orderCommentsTableName),
            array('order_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        ),
        array('order_id')
    );

try {
    $connection->createTable($orderCommentsTable);
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();
