<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:21
 */
class Mageconsult_Dawanda_Block_Sync_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sync_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/sync')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('product_id',
            array(
                'header' => $this->__('Product ID'),
                'index'  => 'product_id'
            )
        );

        $this->addColumn('sku',
            array(
                'header' => $this->__('SKU'),
                'index'  => 'sku',
                'frame_callback' => array($this, 'getUrlLink'),
            )
        );


        $this->addColumn('title',
            array(
                'header' => $this->__('Title'),
                'index'  => 'title',
#                'frame_callback' => array($this, 'getUrlLink'),
            )
        );


        $this->addColumn('magento_data',
            array(
                'header' => $this->__('magento data'),
                'index'  => 'magento_data'
            )
        );


        $this->addColumn('dawanda_data',
            array(
                'header' => $this->__('dawanda_data'),
                'index'  => 'dawanda_data'
            )
        );


        $this->addColumn('status',
            array(
                'header'  => $this->__('status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    Mageconsult_Dawanda_Model_Sync::STATUS_DISABLED      => $this->__('disabled'),
                    Mageconsult_Dawanda_Model_Sync::STATUS_NEW           => $this->__('new'),
                    Mageconsult_Dawanda_Model_Sync::STATUS_STOCKUDDATE   => $this->__('stock update'),
                    Mageconsult_Dawanda_Model_Sync::STATUS_PRODUCTUPDATE => $this->__('full product update'),
                ),
            )
        );


        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getUrlLink($cell, $row)
    {
        $productId = $row->getProductId();
        $url = Mage::getUrl('adminhtml/catalog_product/edit', array('id' => $productId));

        return sprintf('<a href="%s">' . $cell . '</a>', $url);
    }


    public function getRowUrl($row)
    {
        return false;
        #return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/sync')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);


        $this->getMassactionBlock()->addItem('updatestock', array(
            'label' => $this->__('Update stock data with DaWanda'),
            'url'   => $this->getUrl('*/*/updatestock'),
        ));



        $this->getMassactionBlock()->addItem('updatefull', array(
            'label' => $this->__('Update complete data with DaWanda'),
            'url'   => $this->getUrl('*/*/updatefull'),
        ));


        return $this;

    }
}
