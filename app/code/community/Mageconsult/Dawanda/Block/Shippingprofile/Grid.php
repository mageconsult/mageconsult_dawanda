<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 22:31
 */
class Mageconsult_Dawanda_Block_Shippingprofile_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('shippingprofile_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/shippingprofile')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('shippingprofile_id',
            array(
                'header' => $this->__('ID'),
                'index'  => 'shippingprofile_id'
            )
        );

        $this->addColumn('language',
            array(
                'header' => $this->__('Language'),
                'index'  => 'language'
            )
        );

        $this->addColumn('name',
            array(
                'header' => $this->__('Name'),
                'index'  => 'name'
            )
        );

                $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        
                $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));
        
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return false;
        #return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

        protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/shippingprofile')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> $this->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
    }
