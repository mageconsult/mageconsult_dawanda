<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 19:15
 */
class Mageconsult_Dawanda_Block_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'mageconsult_dawanda';
        $this->_controller = 'category';
        $this->_headerText      = $this->__('DaWanda Categories');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

    protected function _prepareLayout()
    {
        $this->removeButton('add');

        $this->_addButton('add_new', array(
            'label'   => Mage::helper('mageconsult_dawanda')->__('Sync with DaWanda'),
            'onclick' => "setLocation('{$this->getUrl('*/*/sync')}')",
            'class'   => 'add'
        ),-10,-10);

        #$this->setChild('grid', $this->getLayout()->createBlock('adminhtml/catalog_product_grid', 'product.grid'));
        return parent::_prepareLayout();
    }

}

