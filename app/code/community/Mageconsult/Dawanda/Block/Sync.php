<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:21
 */
class Mageconsult_Dawanda_Block_Sync extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'mageconsult_dawanda';
        $this->_controller = 'sync';
        $this->_headerText      = $this->__('Sync Data between Magento and DaWanda');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->removeButton('add');

        $this->_addButton('update_stock', array(
            'label'   => Mage::helper('mageconsult_dawanda')->__('Update Stockdata'),
            'onclick' => "setLocation('{$this->getUrl('*/*/updateStock')}')",
            'class'   => 'add'
        ), -10, -20);

        $this->_addButton('generate', array(
            'label'   => Mage::helper('mageconsult_dawanda')->__('Generate Data'),
            'onclick' => "setLocation('{$this->getUrl('*/*/generate')}')",
            'class'   => 'add'
        ), -10, -10);


        #$this->setChild('grid', $this->getLayout()->createBlock('adminhtml/catalog_product_grid', 'product.grid'));
        return parent::_prepareLayout();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

