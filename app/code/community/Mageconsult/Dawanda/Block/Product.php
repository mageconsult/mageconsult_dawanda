<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 23:04
 */
class Mageconsult_Dawanda_Block_Product extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_blockGroup      = 'mageconsult_dawanda';
        $this->_controller      = 'product';
        $this->_headerText      = $this->__('DaWanda Products');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
            }

    protected function _prepareLayout()
    {
        $this->removeButton('add');

        $this->_addButton('sync_with_dawanda', array(
            'label'   => Mage::helper('mageconsult_dawanda')->__('Sync with DaWanda'),
            'onclick' => "setLocation('{$this->getUrl('*/*/sync')}')",
            'class'   => 'add'
        ), -10, -10);

        $this->_addButton('update_local_data', array(
            'label'   => Mage::helper('mageconsult_dawanda')->__('DaWanda => Magento'),
            'onclick' => "setLocation('{$this->getUrl('*/*/updateproducts')}')",
            'class'   => 'add'
        ), -10, -20);

        #$this->setChild('grid', $this->getLayout()->createBlock('adminhtml/catalog_product_grid', 'product.grid'));
        return parent::_prepareLayout();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

