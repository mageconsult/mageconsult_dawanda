<?php

class Mageconsult_Dawanda_Block_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'mageconsult_dawanda';
        $this->_controller = 'log';
        $this->_headerText = $this->__('DaWanda Log');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->removeButton('add');
        return parent::_prepareLayout();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

