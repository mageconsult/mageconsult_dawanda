<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 23:04
 */
class Mageconsult_Dawanda_Block_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('order_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/order')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('order_id',
            array(
                'header' => $this->__('Order ID'),
                'index'  => 'order_id'
            )
        );

        $this->addColumn('username',
            array(
                'header'         => $this->__('Username'),
                'index'          => 'username',
                //'width'          => '40px',
            )
        );

        $this->addColumn('username',
            array(
                'header'         => $this->__('Username'),
                'index'          => 'username',
                //'width'          => '40px',
            )
        );

        $this->addColumn('shipping',
            array(
                'header'         => $this->__('Shipping'),
                'index'          => 'shipping',
                //'width'          => '40px',
                'frame_callback' => array($this, 'decorateShipping'),
            )
        );

        $this->addColumn('invoice',
            array(
                'header'         => $this->__('Invoice'),
                'index'          => 'invoice',
                //'width'          => '40px',
                'frame_callback' => array($this, 'decorateInvoice'),
            )
        );

        $this->addColumn('cents_total',
            array(
                'header'         => $this->__('Amount'),
                'index'          => 'cents_total',
                //'width'          => '40px',
                'frame_callback' => array($this, 'decorateAmount'),
            )
        );

        $this->addColumn('dawanda_status',
            array(
                'header'         => $this->__('Status'),
                'index'          => 'dawanda_status',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/order')->getOptionArray(),
                //'width'          => '40px',
            )
        );

        /*
        $this->addColumn('currency',
            array(
                'header'         => $this->__('Currency'),
                'index'          => 'currency',
                //'width'          => '40px',
            )
        );
        */
        $this->addColumn('dawanda_created_at',
            array(
                'header'         => $this->__('Created at'),
                'index'          => 'dawanda_created_at',
                //'width'          => '40px',
                'frame_callback' => array($this, 'decorateTimestamp'),
            )
        );

        $this->addColumn('dawanda_updated_at',
            array(
                'header'         => $this->__('Updated at'),
                'index'          => 'dawanda_updated_at',
                //'width'          => '40px',
                'frame_callback' => array($this, 'decorateTimestamp'),
            )
        );


        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/order')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url'   => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function decorateAmount($value, $row)
    {
        return $value/100 . ' ' . $row['currency'];
    }


    public function decorateTimestamp($value, $row)
    {
        return $value != '' ? date('d.m.Y H:i:s', $value) : '';
    }

    public function decorateInvoice($value, $row)
    {
        $html = $row['invoice_firstname'] . ' ' . $row['invoice_lastname'] . '<br/>';
        if ($row['invoice_line2'] != '') {
            $html .= $row['invoice_line2'] . '<br/>';
        }
        if ($row['invoice_line3'] != '') {
            $html .= $row['invoice_line3'] . '<br/>';
        }
        $html .= $row['invoice_street'] . '<br/>';
        $html .= $row['invoice_country'] . ' ' . $row['invoice_zip']. ' '. $row['invoice_city'] . '<br/>';

        return $html;
    }

    public function decorateShipping($value, $row)
    {
        $html = $row['shipping_firstname'] . ' ' . $row['shipping_lastname'] . '<br/>';
        if ($row['shipping_line2'] != '') {
            $html .= $row['shipping_line2'] . '<br/>';
        }
        if ($row['shipping_line3'] != '') {
            $html .= $row['shipping_line3'] . '<br/>';
        }
        $html .= $row['shipping_street'] . '<br/>';
        $html .= $row['shipping_country'] . ' ' . $row['shipping_zip']. ' '. $row['shipping_city'] . '<br/>';

        return $html;
    }


    /**
     * Decorate status column values
     *
     * @param $value
     *
     * @return string
     */
    public function decorateStatus($status, $row)
    {
        $status = $row->getData('status');

        switch ($status) {
            case 'active':
                $class = 'notice';
                $value = 'active';
                break;
            case 'paused':
                $class = 'minor';
                $value = 'paused';
                break;
            case 'ended':
                $class = 'critical';
                $value = 'ended';
                break;
            default:
                $class = 'notice';
                $value = 'active';
        }

        $cell = sprintf(
            '<span class="grid-severity-%s"><span>%s</span></span>',
            $class,
            $this->__($value)
        );
        return $cell;
    }
}
