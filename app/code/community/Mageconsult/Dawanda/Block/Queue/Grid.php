<?php

class Mageconsult_Dawanda_Block_Queue_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('queue_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/queue')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('queue_id',
            array(
                'header' => $this->__('#'),
                'index' => 'queue_id'
            )
        );

        $this->addColumn('type',
            array(
                'header' => $this->__('Type'),
                'index' => 'type'
            )
        );

        $this->addColumn('entity_id',
            array(
                'header' => $this->__('ID'),
                'index' => 'entity_id'
            )
        );

        $this->addColumn('state',
            array(
                'header' => $this->__('state'),
                'index' => 'state'
            )
        );

        $this->addColumn('created_at',
            array(
                'header' => $this->__('created_at'),
                'index' => 'created_at',
                'type'   => 'datetime',

            )
        );

        $this->addColumn('executed_at',
            array(
                'header' => $this->__('executed_at'),
                'index' => 'executed_at',
                'type'   => 'datetime',

            )
        );



        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return false;
        #return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/queue')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
