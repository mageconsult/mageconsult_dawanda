<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 23:04
 */
class Mageconsult_Dawanda_Block_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('product_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/product')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('product_id',
            array(
                'header' => $this->__('DaWanda ID'),
                'index'  => 'product_id'
            )
        );


        $this->addColumn('image1',
            array(
                'header'         => $this->__('Img 1'),
                'index'          => 'image1',
                'width'          => '40px',
                'frame_callback' => array($this, 'decorateImage'),
            )
        );

        $this->addColumn('image2',
            array(
                'header'         => $this->__('Img 2'),
                'index'          => 'image2',
                'width'          => '40px',
                'frame_callback' => array($this, 'decorateImage'),
            )
        );

        $this->addColumn('image3',
            array(
                'header'         => $this->__('Img 3'),
                'index'          => 'image3',
                'width'          => '40px',
                'frame_callback' => array($this, 'decorateImage'),
            )
        );

        $this->addColumn('image4',
            array(
                'header'         => $this->__('Img 4'),
                'index'          => 'image4',
                'width'          => '40px',
                'frame_callback' => array($this, 'decorateImage'),
            )
        );

        $this->addColumn('sku',
            array(
                'header' => $this->__('SKU'),
                'index'  => 'sku'
            )
        );

        $this->addColumn('title',
            array(
                'header' => $this->__('Title'),
                'index'  => 'title'
            )
        );

        $this->addColumn('availability',
            array(
                'header' => $this->__('Qty'),
                'index'  => 'availability'
            )
        );

        $this->addColumn('unit',
            array(
                'header' => $this->__('Unit'),
                'index'  => 'unit',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_unit')->getOptionArray(),
            )
        );

        $this->addColumn('cents',
            array(
                'header' => $this->__('cents'),
                'index'  => 'cents'
            )
        );

        $this->addColumn('milli_units_per_item',
            array(
                'header' => $this->__('milli_units_per_item'),
                'index'  => 'milli_units_per_item'
            )
        );

        $this->addColumn('category_id',
            array(
                'header'  => $this->__('Category'),
                'index'   => 'category_id',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_category')->getOptionArray(),

            )
        );

        $this->addColumn('additional_category_id',
            array(
                'header'  => $this->__('2nd Category'),
                'index'   => 'additional_category_id',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_category')->getOptionArray(),

            )
        );

        $this->addColumn('shop_category_id',
            array(
                'header'  => $this->__('Shop Category'),
                'index'   => 'shop_category_id',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_shopcategory')->getOptionArray(),

            )
        );


        $this->addColumn('color1',
            array(
                'header'  => $this->__('Color 1'),
                'index'   => 'color1',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_color')->getOptionArray(),
            ));


        $this->addColumn('color2',
            array(
                'header'  => $this->__('Color 2'),
                'index'   => 'color2',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_color')->getOptionArray(),

            )
        );

        $this->addColumn('mailable_in',
            array(
                'header'  => $this->__('Deliverytime'),
                'index'   => 'mailable_in',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_mailable')->getOptionArray(),
            ));

        $this->addColumn('typology',
            array(
                'header'  => $this->__('typology'),
                'index'   => 'typology',
                'type'    => 'options',
                'options' => Mage::getModel('mageconsult_dawanda/catalog_product_attribute_source_typology')->getOptionArray(),
            ));


        $this->addColumn('status',
            array(
                'header'         => $this->__('Status'),
                'index'          => 'status',
                'type'           => 'options',
                'options'        => array(
                    'active' => $this->__('active'),
                    'paused' => $this->__('paused'),
                    'ended'  => $this->__('ended')
                ),
                'frame_callback' => array($this, 'decorateStatus'),
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/product')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url'   => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function decorateImage($value, $row)
    {
        $imageUrl = $value;
        $out      = ($imageUrl) ? "<img src=" . $imageUrl . " height='40px'/>" : "";

        return $out;
    }

    /**
     * Decorate status column values
     *
     * @param $value
     *
     * @return string
     */
    public function decorateStatus($status, $row)
    {
        $status = $row->getData('status');

        switch ($status) {
            case 'active':
                $class = 'notice';
                $value = 'active';
                break;
            case 'paused':
                $class = 'minor';
                $value = 'paused';
                break;
            case 'ended':
                $class = 'critical';
                $value = 'ended';
                break;
            default:
                $class = 'notice';
                $value = 'active';
        }

        $cell = sprintf(
            '<span class="grid-severity-%s"><span>%s</span></span>',
            $class,
            $this->__($value)
        );
        return $cell;
    }
}
