<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 19:15
 */
class Mageconsult_Dawanda_Block_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('category_grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mageconsult_dawanda/category')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $yesNo = Mage::getModel('adminhtml/system_config_source_yesno')->toArray();

        $this->addColumn('category_id',
            array(
                'header' => $this->__('Category ID'),
                'index'  => 'category_id'
            )
        );

        $this->addColumn('language',
            array(
                'header' => $this->__('Language'),
                'index'  => 'language'
            )
        );

        $this->addColumn('level1',
            array(
                'header' => $this->__('level1'),
                'index'  => 'level1'
            )
        );

        $this->addColumn('level2',
            array(
                'header' => $this->__('level2'),
                'index'  => 'level2'
            )
        );

        $this->addColumn('level3',
            array(
                'header' => $this->__('level3'),
                'index'  => 'level3'
            )
        );
        $this->addColumn('base_price',
            array(
                'type' => 'options',
                'header' => $this->__('base_price'),
                'index'  => 'base_price',
                'options' => $yesNo
            )
        );
        $this->addColumn('combi_buy',
            array(
                'type' => 'options',
                'header' => $this->__('combi_buy'),
                'index'  => 'combi_buy',
                'options'    => $yesNo
            )
        );
        $this->addColumn('created_at',
            array(
                'header' => $this->__('created_at'),
                'type'   => 'datetime',
                'index'  => 'created_at',
                'format' => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            )
        );
        $this->addColumn('updated_at',
            array(
                'header' => $this->__('updated_at'),
                'type'   => 'datetime',
                'index'  => 'updated_at',
                'format' => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return false;
        #return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('mageconsult_dawanda/category')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url'   => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
