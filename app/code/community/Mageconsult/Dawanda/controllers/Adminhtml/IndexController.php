<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 19:15
 */
class Mageconsult_Dawanda_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function syncAllAction() {

        Mage::getModel('mageconsult_dawanda/product')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/category')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/color')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/paymentmethod')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/shippingprofile')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/shopcategory')->updateDataFromDawanda();
        Mage::getModel('mageconsult_dawanda/returnpolicy')->updateDataFromDawanda();

        $this->_getSession()->addSuccess(
            Mage::helper('mageconsult_dawanda')->__('Done.')
        );

        $this->_redirect('adminhtml/dashboard/index');
    }


    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('dawanda/index');
    }


}