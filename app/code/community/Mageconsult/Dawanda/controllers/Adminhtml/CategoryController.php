<?php

require_once Mage::getModuleDir('controllers', 'Mageconsult_Dawanda').DS.'Adminhtml/AbstractController.php';


/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 02.02.16
 * Time: 19:15
 */
class Mageconsult_Dawanda_Adminhtml_CategoryController extends Mageconsult_Dawanda_Adminhtml_AbstractController
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/category'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Category_export.csv';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/category_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Category_export.xml';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/category_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Category(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('mageconsult_dawanda/category')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('mageconsult_dawanda/category');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('This Category no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('category_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/category_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id    = $this->getRequest()->getParam('id');
            $model = Mage::getModel('mageconsult_dawanda/category');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('mageconsult_dawanda')->__('This Category no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Category has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('mageconsult_dawanda')->__('Unable to save the Category.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('mageconsult_dawanda/category');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('mageconsult_dawanda')->__('Unable to find a Category to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Category has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while deleting Category data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('mageconsult_dawanda')->__('Unable to find a Category to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }


    public function syncAction()
    {

        $timer_start = microtime(true);
        $error = Mage::getModel('mageconsult_dawanda/category')->updateDataFromDawanda();
        $timer_stop = microtime(true);

        if ($error !== false) {
            $this->_getSession()->addError(
                Mage::helper('mageconsult_dawanda')->__('Error: %s ', $error)
            );

        }
        else {
            $this->_getSession()->addSuccess(
                Mage::helper('mageconsult_dawanda')->__('Updated Dawanda Categories in ' . number_format($timer_stop-$timer_start,2) . ' seconds.')
            );
        }


        $this->_redirect('*/*/index');
    }

    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('dawanda/category');
    }


}