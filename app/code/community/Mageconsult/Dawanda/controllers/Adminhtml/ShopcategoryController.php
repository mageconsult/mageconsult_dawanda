<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 20:55
 */

require_once Mage::getModuleDir('controllers', 'Mageconsult_Dawanda').DS.'Adminhtml/AbstractController.php';


class Mageconsult_Dawanda_Adminhtml_ShopcategoryController extends Mageconsult_Dawanda_Adminhtml_AbstractController {

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/shopcategory'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Shopcategory_export.csv';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/shopcategory_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Shopcategory_export.xml';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/shopcategory_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Shopcategory(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('mageconsult_dawanda/shopcategory')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('mageconsult_dawanda/shopcategory');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('This Shopcategory no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('shopcategory_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/shopcategory_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id    = $this->getRequest()->getParam('id');
            $model = Mage::getModel('mageconsult_dawanda/shopcategory');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('mageconsult_dawanda')->__('This Shopcategory no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Shopcategory has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('mageconsult_dawanda')->__('Unable to save the Shopcategory.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('mageconsult_dawanda/shopcategory');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('mageconsult_dawanda')->__('Unable to find a Shopcategory to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Shopcategory has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while deleting Shopcategory data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('mageconsult_dawanda')->__('Unable to find a Shopcategory to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }



    public function syncAction()
    {

        Mage::getModel('mageconsult_dawanda/shopcategory')->updateDataFromDawanda();

        $this->_redirect('*/*/index');
    }

    public function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('dawanda/shopcategory');
    }
}