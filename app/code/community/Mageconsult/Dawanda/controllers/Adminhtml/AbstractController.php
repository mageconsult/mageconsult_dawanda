<?php

/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 03.02.16
 * Time: 22:11
 */
class Mageconsult_Dawanda_Adminhtml_AbstractController extends Mage_Adminhtml_Controller_Action
{

    public function preDispatch()
    {

        parent::preDispatch();

        if ($errors = $this->_validate()) {
            foreach ($errors as $error) {
                $this->_getSession()->addError($error);
            }
        }

        return $this;
    }

    protected function _validate() {

        return Mage::getModel('mageconsult_dawanda/dawanda')->validate();
    }

}