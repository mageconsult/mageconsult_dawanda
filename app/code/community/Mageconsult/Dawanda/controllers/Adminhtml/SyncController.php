<?php
/**
 * Created by PhpStorm.
 * User: ralf
 * Date: 14.02.16
 * Time: 23:21
 */

require_once Mage::getModuleDir('controllers', 'Mageconsult_Dawanda') . DS . 'Adminhtml/AbstractController.php';

class Mageconsult_Dawanda_Adminhtml_SyncController extends Mageconsult_Dawanda_Adminhtml_AbstractController
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/sync'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Sync_export.csv';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/sync_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Sync_export.xml';
        $content  = $this->getLayout()->createBlock('mageconsult_dawanda/sync_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Sync(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('mageconsult_dawanda/sync')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('mageconsult_dawanda/sync');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('This Sync no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('sync_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mageconsult_dawanda/sync_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id    = $this->getRequest()->getParam('id');
            $model = Mage::getModel('mageconsult_dawanda/sync');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('mageconsult_dawanda')->__('This Sync no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Sync has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('mageconsult_dawanda')->__('Unable to save the Sync.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('mageconsult_dawanda/sync');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('mageconsult_dawanda')->__('Unable to find a Sync to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('mageconsult_dawanda')->__('The Sync has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while deleting Sync data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('mageconsult_dawanda')->__('Unable to find a Sync to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }

    public function generateAction()
    {
        $result = Mage::getModel('mageconsult_dawanda/sync')->generate();

        // go to grid
        $this->_redirect('*/*/index');
    }


    public function updatestockAction() {

        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Products(s).'));
        } else {
            try {
                foreach ($ids as $id) {

                    $syncModel = Mage::getModel('mageconsult_dawanda/sync')->load($id);
                    if ($syncModel->getId()) {
                        $product = Mage::getModel('catalog/product')->load((int) $syncModel->getData('product_id'));
                        $dawandaProduct  = Mage::getModel('mageconsult_dawanda/query_productAttributeUpdate');
                        $sku = $syncModel->getData('sku');
                        $qty = $syncModel->getData('magento_data');

                        $result = $dawandaProduct->updateAvailabilty($sku, $qty);

                        $syncModel->setData('dawanda_data', $qty);
                        $syncModel->setStatus('');
                        $syncModel->save();

                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }

        // go to grid
        $this->_redirect('*/*/index');

    }

    /**
     * send full data to DaWanda
     */
    public function updatefullAction() {

        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Sync(s).'));
        } else {
            try {
                foreach ($ids as $id) {

                    $syncModel = Mage::getModel('mageconsult_dawanda/sync')->load($id);
                    if ($syncModel->getId()) {
                        $product = Mage::getModel('catalog/product')->load((int) $syncModel->getData('product_id'));
                        $dawandaProduct  = Mage::getModel('mageconsult_dawanda/query_product');

                        $result = $dawandaProduct->createDawandaProduct($product);

                        #$syncModel->setData('dawanda_data', $qty);
                        #$syncModel->setStatus('');
                        #$syncModel->save();

                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }

        // go to grid
        $this->_redirect('*/*/index');

    }


    /**
     *
     */
    public function createDawandaProducts() {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Sync(s).'));
        } else {
            try {
                foreach ($ids as $id) {

                    $syncModel = Mage::getModel('mageconsult_dawanda/sync')->load($id);
                    if ($syncModel->getId()) {
                        $product = Mage::getModel('catalog/product')->load((int) $syncModel->getData('product_id'));
                        $dawandaProduct  = Mage::getModel('mageconsult_dawanda/query_product');
                        $dawandaProduct->setProduct($product);
                        $result = $dawandaProduct->updateAvailabilty();
                    }
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been created/updated.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('mageconsult_dawanda')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }

        // go to grid
        $this->_redirect('*/*/index');
    }

}